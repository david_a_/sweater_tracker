import sys, os
sys.path.append(os.path.join(sys.path[0],'lib'))
from bs4 import BeautifulSoup
import urllib
import json

class Sweater:
  sweater_count = 0


  def __init__(self, url):
    r = urllib.urlopen(url)
    soup = BeautifulSoup(r, "html.parser")

    self.url = url.strip()
    self.id = self.url.split('/')[6].split('?')[0].strip()
    self.slug = self.url.split('/')[5].strip()
    self.price = soup.find(attrs={"name":"product:price:amount"})['content'].strip()
    self.friendly_name = soup.find(attrs={"id":"product-name"}).get_text().strip()

    Sweater.sweater_count += 1


  def get_url(self): return self.url
  def set_url(self, url): self.url = url 

  def get_id(self): return self.id
  def set_id(self, id): self.id = id

  def get_slug(self): return self.slug
  def set_url(self, slug): self.slug = slug

  def get_url(self): return self.price
  def set_url(self, price): self.price = price

  def get_url(self): return self.friendly_name
  def set_url(self, friendly_name): self.friendly_name = friendly_name

  def get_json(self):
    return json.dumps(self.__dict__)

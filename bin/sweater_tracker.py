#!/usr/bin/env python
import sys, os
sys.path.append(os.path.join(sys.path[0],'lib'))
from Sweaters import Sweater


def main(argv):

  try:
    sweater_urls = open(argv[1], 'r')
  except:
    sweater_urls = open(os.path.join(sys.path[0],'sweaters.conf'), 'r')

  for url in sweater_urls:
    print Sweater(url).get_json()


if __name__ == '__main__':
  sys.exit(main(sys.argv))
